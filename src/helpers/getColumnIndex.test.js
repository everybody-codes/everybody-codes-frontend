import getColumnIndex from './getColumnIndex';

test('pure function returns the same output for the same input', () => {
  expect(getColumnIndex(15)).toBe(2);
  expect(getColumnIndex(3)).toBe(0);
  expect(getColumnIndex(5)).toBe(1);
  expect(getColumnIndex(7)).toBe(3);
  expect(getColumnIndex(501)).toBe(0);
  expect(getColumnIndex(508)).toBe(3);
  expect(getColumnIndex(741)).toBe(0);
  expect(getColumnIndex(760)).toBe(1);
});

const getColumnIndex = (number) => {
  // FizzBuzz time!
  if(number % 3 === 0 && number % 5 === 0) {
    return 2;
  } else if (number % 3 === 0) {
    return 0;
  } else if (number % 5 === 0) {
    return 1;
  } else {
    return 3;
  }
};

export default getColumnIndex;

import React from 'react';
import styled from 'styled-components';

const Heading = styled.div`
  background-color: #272727;
  color: #7f7f7c;
  font-size: 3em;
  height: 75px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: bold;
`;

const Table = styled.table`
  width: 100%;
  padding: 10px;
`;

const Th = styled.th`
  padding: 3px 0;
  text-align: left;
  font-size: 12px;
`;

const Td = styled.td`
  font-size: 12px;
`;

function CameraData(props) {
  const { title, data } = props;
  const records = data.map(v => (
      // id as key is not possible, since id's are not unique
      <tr key={v.name}>
        <Td>{v.id}</Td>
        <Td>{v.name}</Td>
        <Td>{v.lat}</Td>
        <Td>{v.lng}</Td>
      </tr>
  ));
  return (
      <div>
        <Heading>{title}</Heading>
        <Table>
          <thead>
            <tr>
              <Th>Number</Th>
              <Th>Name</Th>
              <Th>Latitude</Th>
              <Th>Longitude</Th>
            </tr>
          </thead>
          <tbody>
            {records}
          </tbody>
        </Table>
      </div>
  )
}

export default CameraData;

import React from 'react';
import cameraIcon from '../assets/images/video-camera-icon.png';

function Marker(props) {
  return (
      <img
          src={cameraIcon}
          style={{width: 10, height: 10}}
          alt=""
      />
  )
}

export default Marker;

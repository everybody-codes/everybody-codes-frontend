import React from 'react';
import GoogleMapReact from "google-map-react";
import Marker from "./Marker";

function Map(props) {
  const Markers = props.data.map(v => (
      <Marker key={v.name} lat={v.lat} lng={v.lng} />
  ));

  return (
      <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyDTlMBrVyHb5M5NKHaJiSbGZmstXW5PpN0" }}
          defaultCenter={{
            lat: 52.0905331,
            lng: 5.1183209
          }}
          defaultZoom={14}
      >
        {Markers}
      </GoogleMapReact>
  )
}

export default Map;

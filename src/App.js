import React from 'react';
import './App.css';
import Map from "./components/Map";
import CameraData from "./components/CameraData";
import getColumnIndex from "./helpers/getColumnIndex";
import styled from 'styled-components';

const Grid = styled.div`
  display: grid;
  grid-template-columns: auto 500px;
  grid-template-rows: 1fr;
  height: 100vh;
`;

const Heading = styled.div`
  position: absolute;
  background: rgba(255,255,255,.75);
  padding: 10px 20px;
  font-size: 18px;
  font-weight: bold;
  left: 10px;
  top: 10px;
  border-radius: 3px;
  z-index: 99;
`;

const SideBar = styled.div`
  overflow-y: scroll;
`;

class App extends React.Component {
  state = {
    cameras: [],
    cameras3: [],
    cameras5: [],
    cameras35: [],
    camerasOther: [],
  };

  componentDidMount() {
    fetch('http://localhost:5000')
        .then(response => response.json())
        .then(data => {
          this.setState({
            cameras: data,
            cameras3: data.filter(v => getColumnIndex(v.id) === 0),
            cameras5: data.filter(v => getColumnIndex(v.id) === 1),
            cameras35: data.filter(v => getColumnIndex(v.id) === 2),
            camerasOther: data.filter(v => getColumnIndex(v.id) === 3),
          });
        })
        .catch(error => console.error(error));
  }

  render() {
    return (
        <Grid>
          <Map data={this.state.cameras} />
          <SideBar>
            <CameraData title="Cameras 3" data={this.state.cameras3} />
            <CameraData title="Cameras 5" data={this.state.cameras5} />
            <CameraData title="Cameras 3 & 5" data={this.state.cameras35} />
            <CameraData title="Cameras overig" data={this.state.camerasOther} />
          </SideBar>
          <Heading>Security cameras Utrecht</Heading>
        </Grid>
    );
  }
}

export default App;

# everybody-codes-frontend

Deze repo bevat mijn uitwerking voor het frontend deel van de Everybody Codes opdracht.

## Frontend opstarten
- Draai de server uit de https://gitlab.com/everybody-codes/everybody-codes-api repo
- `yarn install` of `npm install`
- `npm start`

## Tests draaien
- `npm run test`
- `a`

## NSFAQ (Not So Frequently Asked Questions)
**Vraag:** React voor zo'n eenvoudige pagina, vind je dat niet een beetje overkill?

**Antwoord:** Tuurlijk! Maar ik vind component based werken wel tof. En ik wilde even demonstreren wat ik in grofweg een uurt in React in elkaar draai.
##
**Vraag:** Deze app ziet er totaal anders uit dan het aangeleverde voorbeeld, waarom?

**Antwoord:** Ik vond de indeling in de oorspronkelijke HTML niet echt logisch, een sidebar leek bij praktischer met zo'n bak data. Dus ook hier: ik wilde even aantonen dat met een kleine moeite een pagina uit de grond gestampt kan worden.
##
**Vraag:** Ben je niet iets vergeten?

**Antwoord:** Ik heb geen typechecking toegepast. Ik had het project in TypeScript op moeten zetten en had geen tijd meer om dit achteraf te doen.
